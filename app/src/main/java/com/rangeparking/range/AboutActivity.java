package com.rangeparking.range;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.like.LikeButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;

public class AboutActivity extends AppCompatActivity implements OnMapReadyCallback{

//    Constants
    private static final String TAG = AboutActivity.class.getSimpleName();
    private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";
    private static final float DEFAULT_ZOOM = 15f;

//    Google Maps API
    private GoogleMap mMap;
    private MapView mMapView;

//    Firebase
    FirebaseFirestore mFirestore = FirebaseFirestore.getInstance();
//    TODO update the method by which suggested spaces are listed
    CollectionReference mListingsRef = mFirestore.collection("listings");

//    Widgets
    private Toolbar mToolbar;
    private LinearLayout mTagsContainer;
    private LinearLayout mReviewsContainer;
    private ConstraintLayout mAboutBtn;
    private RecyclerView similarSpaces;
    private TextView mSuggestionLocationText;
    private TextView mDescriptionText;
    private TextView mOwnerName;
    private TextView mReadMore;
    private TextView mReviewsCount;
    private TextView mListingTitle;
    private CircleImageView mOwnerProfilePicture;
    private ProgressBar mOwnerProfilePictureProgress;
    private RatingBar listingRating;
    private ViewPager mImageSlider;

//    Vars
    private List<Listing> mListings;
    private Listing mListing;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        mToolbar = findViewById(R.id.about_app_bar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_left);
        mToolbar.setNavigationOnClickListener(v -> {onBackPressed();});

        mSuggestionLocationText = findViewById(R.id.about_suggentions_location_text);
        mDescriptionText = findViewById(R.id.about_listing_description);
        mOwnerName = findViewById(R.id.about_listing_owner_name);
        mOwnerProfilePicture = findViewById(R.id.about_listing_profile_picture);
        mOwnerProfilePictureProgress = findViewById(R.id.about_propic_progressBar);
        mReadMore = findViewById(R.id.about_description_read_more);
        listingRating = findViewById(R.id.ratingBar);
        mReviewsCount = findViewById(R.id.about_listing_reviews_count);
        mImageSlider = findViewById(R.id.about_image_slider);
        mListingTitle = findViewById(R.id.about_listing_title);
        mReviewsContainer = findViewById(R.id.about_reviews_linear_layout);

        mTagsContainer = findViewById(R.id.tags_contatiner);
//        get Listings from intent to display suggestions
        mListings = getIntent().getParcelableArrayListExtra("listings");
//        get index of selected listing
        int pos = getIntent().getIntExtra("pos", -1);
        mListing = mListings.get(pos);

//        Same Listing should not be shown in similar listings
        mListings.remove(mListing);

        ImageAdapter imageAdapter = new ImageAdapter(this, mListing.getPictures());
        mImageSlider.setAdapter(imageAdapter);

        mListingTitle.setText(mListing.getTitle());

        listingRating.setRating(mListing.getRating());
        mReviewsCount.setText("(" + mListing.getReviews() + ")");

//        Populates Linear Layout in ScrollView with tags
        createTags();
//        reterives owner data from firestore
        getCreatorInfo();
        mDescriptionText.setText(mListing.getDescription());
//        shows full description
        mReadMore.setOnClickListener(v -> {
            mDescriptionText.setMaxLines(Integer.MAX_VALUE);
            mReadMore.setVisibility(View.INVISIBLE);
        });

        // Get Map View Bundle
        Bundle mapViewBundle = null;
        if(savedInstanceState != null) { mapViewBundle =
                savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY); }

        // Gets Map View from XML and creates it.
        mMapView = findViewById(R.id.about_mapView);
        Log.d(TAG, "mMapView.onCreate called");
        mMapView.onCreate(mapViewBundle);

        mMapView.getMapAsync(this);

        getListingReviews();

        similarSpaces = findViewById(R.id.about_similiar_listings);
        similarSpaces.setHasFixedSize(true);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        similarSpaces.setLayoutManager(manager);

        SnapHelper helper = new LinearSnapHelper();
        helper.attachToRecyclerView(similarSpaces);

        ListingsAdapter listingAdapter = new ListingsAdapter(this, mListings);
        similarSpaces.setAdapter(listingAdapter);

        similarSpaces.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    View centerView = helper.findSnapView(manager);
                    position = manager.getPosition(centerView);
                    Listing currentListing = mListings.get(position);
                   mSuggestionLocationText.setText(currentListing.getStreetAddress());
                }
            }
        });

        mAboutBtn = findViewById(R.id.about_about_button);
        mAboutBtn.setOnClickListener(clickListener);
    }

    private void getListingReviews() {
        mListingsRef.document(mListing.getId()).collection("topReviews")
                .get().addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        QuerySnapshot snapshot = task.getResult();
                        for (DocumentSnapshot d :snapshot) {
                            populateReviewsContainer(d.getData());
                        }
                    }
                });
    }

    private void populateReviewsContainer(Map<String, Object> review) {

        LayoutInflater inflater = getLayoutInflater();
        View reviewView = inflater.inflate(R.layout.review_layout, null);


        TextView reviewerName = reviewView.findViewById(R.id.review_name);
        TextView reviewTimeStamp = reviewView.findViewById(R.id.review_timestamp);
        TextView reviewBody = reviewView.findViewById(R.id.review_body);
        TextView reviewLikeText = reviewView.findViewById(R.id.review_like_text);
        ImageButton reviewLikeBtn = reviewView.findViewById(R.id.review_like_btn);
        ProgressBar reviewProgress = reviewView.findViewById(R.id.review_picture_progress);
        CircleImageView reviewPicture = reviewView.findViewById(R.id.review_picture);
        RatingBar reviewRating = reviewView.findViewById(R.id.review_ratingBar);

        reviewRating.setRating(new Float(review.get("rating").toString()));
        reviewTimeStamp.setText(review.get("timestamp").toString());
        reviewBody.setText(review.get("review").toString());

        reviewLikeBtn.setOnClickListener(v -> {
            reviewLikeBtn.setSelected(!reviewLikeBtn.isSelected());

            if(reviewLikeBtn.isSelected()) reviewLikeText.setVisibility(View.INVISIBLE);
            else reviewLikeText.setVisibility(View.VISIBLE);
        });

        mFirestore.collection("users")
                .document(review.get("reviewer").toString())
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        DocumentSnapshot snapshot = task.getResult();
                        if(snapshot.exists()){
                            User reviewer = snapshot.toObject(User.class);
                            reviewerName.setText(
                                    reviewer.getFirstName() +
                                    " " +
                                    reviewer.getLastName());
                            Glide
                                .with(this)
                                .load(reviewer.getProfilePictureUrl())
                                .apply(new RequestOptions()
                                .centerCrop())
                                .listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e,
                                                                Object model,
                                                                Target<Drawable> target,
                                                                boolean isFirstResource) {
                                        reviewProgress.setVisibility(View.GONE);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model,
                                                                   Target<Drawable> target,
                                                                   DataSource dataSource,
                                                                   boolean isFirstResource) {
                                        reviewProgress.setVisibility(View.GONE);
                                        return false;
                                    }
                                })
                                .into(reviewPicture);
                        }
                    }
                });

        mReviewsContainer.addView(reviewView);
    }

    private void getCreatorInfo() {
        mFirestore.collection("users")
                .document(mListing.getCreator())
                .get()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        DocumentSnapshot document = task.getResult();
                        if(document.exists()) populateCreatorInfo(document.toObject(User.class));
                        else Toast.makeText(this,"document doesnt exist", Toast.LENGTH_LONG).show();
                    } else Toast.makeText(this, "task failed", Toast.LENGTH_LONG).show();
                });
    }

    private void populateCreatorInfo(User user) {
        mOwnerName.setText(user.getFirstName() + " " + user.getLastName());
        Glide
            .with(this)
            .load(user.getProfilePictureUrl())
            .apply(new RequestOptions()
            .centerCrop())
            .listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    mOwnerProfilePictureProgress.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    mOwnerProfilePictureProgress.setVisibility(View.GONE);
                    return false;
                }
            })
            .into(mOwnerProfilePicture);
    }

    private View.OnClickListener clickListener = v -> {
        switch (v.getId()){

            case R.id.about_about_button:
                startNewAbout();
                break;

            default:
                break;
        }
    };

    private void startNewAbout() {
        mListings.add(mListing);
        Intent newAboutIntent = getIntent();
        overridePendingTransition(0, 0);
//        newAboutIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        newAboutIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        newAboutIntent.putExtra("pos", position);
        newAboutIntent.putParcelableArrayListExtra("listings",
                (ArrayList<? extends Parcelable>) mListings);
        finish();
        overridePendingTransition(0, 0);
        startActivity(newAboutIntent);
    }

    private void createTags() {
        LayoutInflater inflater = getLayoutInflater();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMarginStart(10);
        params.setMarginEnd(10);
        Set<String> tags = mListing.getTags().keySet();
        for(String tag : tags){
            TextView tagView =  (TextView) inflater.inflate(R.layout.tag, null);
            tagView.setText(tag);
            tagView.setLayoutParams(params);
            mTagsContainer.addView(tagView);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        styleMap();
        LatLng l = new LatLng(mListing.getLocation().getLatitude(),
                mListing.getLocation().getLongitude());
        moveCamera(l, DEFAULT_ZOOM, false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapVieBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if(mapVieBundle != null){
            mapVieBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapVieBundle);
        }

        mMapView.onSaveInstanceState(mapVieBundle);
    }

    private void styleMap(){
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.map_style));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
    }

    private void moveCamera(LatLng latLng, float zoom, Boolean animate){
        if(animate) mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        else mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }
}
