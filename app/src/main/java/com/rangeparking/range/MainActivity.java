package com.rangeparking.range;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

//    Constants


//    Widgets
    private ParkingMapFragment mParkingMapFragment;
    private MessagesFragment mMessagesFragment;
    private SearchFragment mSearchFragment;
    private BottomNavigationView mBottomNav;
    private FrameLayout mMainFrameLayout;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private Toolbar mToolbar;
    private NavigationView mNavigationView;

//    Firebase
    private FirebaseAuth mAuth;
//    Vars
//    Typeface arimo = Typeface.createFromFile("fonts/arimo.xml");



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();


        mBottomNav = findViewById(R.id.bottomNavigationView);
        mMainFrameLayout = findViewById(R.id.main_frame_layout);


        mDrawerLayout = findViewById(R.id.main_drawer);

        mParkingMapFragment = new ParkingMapFragment();
        mSearchFragment = new SearchFragment();
        mMessagesFragment = new MessagesFragment();

        setFragment(mParkingMapFragment);
        int id = mBottomNav.getMenu().getItem(1).getItemId();
        mBottomNav.setSelectedItemId(id);

        mBottomNav.setOnNavigationItemSelectedListener(bottomNavListner);
        mNavigationView = findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);


    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser == null) goToWelcomeActivity();
    }

    private void goToWelcomeActivity() {
        Intent welcomeActivityIntent = new Intent(MainActivity.this, WelcomeActivity.class);
        startActivity(welcomeActivityIntent);
        finish();
    }

    BottomNavigationView.OnNavigationItemSelectedListener bottomNavListner = item -> {

        switch (item.getItemId()){

            case R.id.bottom_nav_nearby:
                setFragment(mParkingMapFragment);
                return true;

            case R.id.bottom_nav_messages:
                setFragment(mMessagesFragment);
                return true;

            case R.id.bottom_nav_spaces:
                setFragment(mSearchFragment);
                return true;

            default:
                return false;
        }
    };

    private void setFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_frame_layout, fragment)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mToggle.onOptionsItemSelected(item)) return true;

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.drawer_log_out:
                mAuth.signOut();
                goToWelcomeActivity();
                break;
            default:
                break;
        }

        return false;
    }

//    private void setNavigationItemFont(){
//
//        for(int i = 0; i < mBottomNav.getMenu().size(); i++){
//            MenuItem menuItem = mBottomNav.getMenu().getItem(i);
//            SpannableStringBuilder titleSpan = new SpannableStringBuilder(menuItem.getTitle());
//
//        }
//
//    }

    public void openNavDrawer(){
        mDrawerLayout.openDrawer(mNavigationView);
    }

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }
}
