package com.rangeparking.range;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class ParkingMapFragment extends Fragment implements OnMapReadyCallback {

    //    Constants
    private static final String TAG = ParkingMapFragment.class.getSimpleName();
    private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private static final float DEFAULT_ZOOM = 15f;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;


    //    Google API vars
    private GoogleMap mMap;
    private MapView mMapView;
    private FusedLocationProviderClient mFusedLocationClient;

    //    Views
    private FloatingActionButton mMyLocationButton;
    private FloatingActionButton mFilterOptionsButton;
    private FiltersBottomSheetDialog mBottomSheetDialog;
    private AppCompatImageButton mMenuButton;
    private EditText mSearchField;

    //    Other
//    protected Location mLastLocation;
    private boolean mapNotConfigured = true;



    public ParkingMapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_parking_map, container, false);

        // Get Map View Bundle
        Bundle mapViewBundle = null;
        if(savedInstanceState != null) { mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY); }

        // Gets Map View from XML and creates it.
        mMapView = v.findViewById(R.id.mapView);
        Log.d(TAG, "mMapView.onCreate called");
        mMapView.onCreate(mapViewBundle);

//        ViewGroup.LayoutParams parms = mMapView.getLayoutParams();
//        parms.height = 900;
//        mMapView.setLayoutParams(parms);
        mMapView.getMapAsync(this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        mMyLocationButton = v.findViewById(R.id.myLocationFloatingActionButton);
        mFilterOptionsButton = v.findViewById(R.id.filterOptionsFloatingActionButton);
        mMenuButton = v.findViewById(R.id.search_bar_menu_button);
        mSearchField = v.findViewById(R.id.search_bar_text_feild);
        mMyLocationButton.setOnClickListener(mButtonListner);
        mFilterOptionsButton.setOnClickListener(mButtonListner);
        mMenuButton.setOnClickListener(mButtonListner);
        mSearchField.setOnClickListener(mButtonListner);

        mBottomSheetDialog = new FiltersBottomSheetDialog();

        return v;
    }

    /**
     * OnClick listener for button click events
     */
    private View.OnClickListener mButtonListner = (view) -> {
        switch (view.getId()){
            case R.id.myLocationFloatingActionButton: getLastLocation(true);
                break;
            case R.id.filterOptionsFloatingActionButton: //TODO implemnt filter bottom sheet
                mBottomSheetDialog.show(getActivity().getSupportFragmentManager(), "filterBottomSheet");
                break;
            case R.id.search_bar_menu_button:
                ((MainActivity) getActivity()).openNavDrawer();
                break;
            case R.id.search_bar_text_feild:
                beginSearch();
                break;
            default:
                break;
        }
    };



    private void beginSearch() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(getActivity());
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
            Toast.makeText(getContext(), "Error", Toast.LENGTH_LONG).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
            Toast.makeText(getContext(), "Error:", Toast.LENGTH_LONG).show();

        }
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapVieBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if(mapVieBundle != null){
            mapVieBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapVieBundle);
        }

        mMapView.onSaveInstanceState(mapVieBundle);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    /**
     * Check if permissions are granted, if not requests permissions
     * else gets the location of device and configures map
     */

    @Override
    public void onStart() {
        super.onStart();
        mMapView.onStart();

        if(!checkPermissions()){
            requestPermissions();
        }else {
            getLocationAndConfigureMap();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    /**
     * Permissions are checked by checkPermissions Method Call
     * Constructs Google Map and if location permissions are granted
     * and Map is not yet configured, configures map
     * @param googleMap
     */
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        styleMap();
        if(checkPermissions()) configureMap();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    /**
     * Checks if location permissions have been granted, if not
     * shows rationale and asks user for permission again. Else
     * gets the location of device and configures Google Map
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_PERMISSIONS_REQUEST_CODE){
            if(grantResults.length <= 0){
                Log.i(TAG, "User interaction was cancelled");
            } else {
                boolean permissionsDenied = false;
                for (int permission: grantResults) {
                    if(permission != PackageManager.PERMISSION_GRANTED){
                        permissionsDenied = true;
                        break;
                    }
                }
                if(permissionsDenied){
                    showSnackbar(R.string.permissionRationale, R.string.settings,
                            (view) -> {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            });
                } else {
                    getLocationAndConfigureMap();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                Log.i(TAG, "Place: " + place.getName());
                goToSearchActivity(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                // TODO: Handle the error.
                Toast.makeText(getContext(),"Error", Toast.LENGTH_LONG).show();
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
                Toast.makeText(getContext(), "Canclled", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Displays Snack Bar with text
     * @param text  Text for the body of Snack Bar
     */
    private void showSnackbar(final String text) {
        View container = getActivity().findViewById(R.id.mapView_container);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * Creates a Snack Bar with action
     * @param mainTextStringId  String resource of Snack Bar body
     * @param actionStringId    String resource of action Button
     * @param listener          OnClick listener for action button click events
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(getActivity().findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }


    /**
     * Checks if the user granted location permissions
     * @return Boolean representing whether the user granted location permissions
     */
    private boolean checkPermissions() {
        int fineLocationPermissionState = ActivityCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        int coarseLocationPermissionState = ActivityCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_COARSE_LOCATION);
        return fineLocationPermissionState == PackageManager.PERMISSION_GRANTED
                && coarseLocationPermissionState == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Requests the user for location permissions
     */
    private void startLocationPermissionsRequest(){
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    /**
     * Requests user for location permissions.
     * If user previously denied permissions, shows rationale
     * for allowing permission.
     */
    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION);

        if(shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permissionRationale, android.R.string.ok,
                    (View view) -> {
                        // Request permission
                        startLocationPermissionsRequest();
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            startLocationPermissionsRequest();
        }
    }

    /**
     * Method called only after permission check
     * Retrieves the last known location of device
     */
    @SuppressLint("MissingPermission")
    private void getLastLocation(Boolean animate) {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        Location lastLocation = task.getResult();
                        moveCamera(lastLocation, animate);

                    } else {
                        Log.w(TAG, "getLastLocation:exception", task.getException());
                        showSnackbar(getString(R.string.no_location_detected));
                    }
                });
    }

    private void getLastLocation(){
        getLastLocation(false);
    }

    /**
     * moves Camera to specified location
     * @param location  Location where the camera should move
     */
    private void moveCamera(Location location, Boolean animate){
        moveCamera(new LatLng(location.getLatitude(), location.getLongitude()), DEFAULT_ZOOM, animate);
    }

    /**
     * Moves camera to specified latLang and zoom
     * @param latLng The LatLang representing the users current position
     * @param zoom The zoom value for when the camera moves
     */
    private void moveCamera(LatLng latLng, float zoom, Boolean animate){
        if(animate) mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        else mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    /**
     * Gets device last location and configures GoogleMap
     */
    private void getLocationAndConfigureMap(){
        getLastLocation();
        configureMap();
    }

    /**
     * Method is called only after permission checks.
     * If GoogleMap not null and is not configured,
     * determined by mapNotConfigured var, sets map
     * properties.
     */
    @SuppressLint("MissingPermission")
    private void configureMap(){
        if(mapNotConfigured && mMap != null){
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mapNotConfigured = false;
        }
    }

    private void styleMap(){
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.map_style));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
    }

    private void goToSearchActivity(Place place){
        Intent searchActivityIntent = new Intent(getActivity(), SearchActivity.class);
        searchActivityIntent.putExtra("latLng", place.getLatLng());
        startActivity(searchActivityIntent);
    }
}
