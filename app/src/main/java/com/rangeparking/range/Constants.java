package com.rangeparking.range;

public final class Constants {

    public static class Tags{

        public static final String ALL_DAY_PARKING = "All Day Parking";

        public static final String DRIVEWAY = "Drive way";

        public static final String CONDO = "Condo";

        public static final String SEDAN = "Sedan";

        public static final String SUV = "SUV";

        public static final String TRUCK = "Truck";

        public static final String MOTORCYCLE = "Motorcycle";

    }

}
