package com.rangeparking.range;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchActivity extends AppCompatActivity implements OnMapReadyCallback{

//    Constants
    private static final String TAG = SearchActivity.class.getSimpleName();
    private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";
    private static final float DEFAULT_ZOOM = 15f;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

//    Google Maps API
    private GoogleMap mMap;
    private LatLng mPlace;

//    Widgets
    private MapView mMapView;
    private RecyclerView mListingsView;
    private ConstraintLayout mAboutBtn;
    private TextView mListingBookNow;
    private TextView mLocationText;
    private FloatingActionButton mCenterLocationBtn;
    private Toolbar mToolbar;

//    Firebase
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFirestore = FirebaseFirestore.getInstance();
    private CollectionReference listingsRef= mFirestore.collection("listings");

//    Vars
    private ListingsAdapter listingsAdapter;
    private List<Listing> listings;
    private Map<String, Listing> listingsMap;
    private int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);


        Intent searchActivityIntent = getIntent();
            mPlace = searchActivityIntent.getExtras().getParcelable("latLng");

        mToolbar = findViewById(R.id.search_toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_left);

        mAuth = FirebaseAuth.getInstance();
        listings = new ArrayList<>();
        listingsMap = new HashMap<>();

        mLocationText = findViewById(R.id.search_listings_location_text);

        mAboutBtn = findViewById(R.id.search_listing_about_button);
        mListingBookNow = findViewById(R.id.search_listing_book_now_text);
        mCenterLocationBtn = findViewById(R.id.search_locate_fab);

        mAboutBtn.setOnClickListener(listener);
        mListingBookNow.setOnClickListener(listener);
        mCenterLocationBtn.setOnClickListener(listener);

        mListingsView = findViewById(R.id.listings_view);
        mListingsView.setHasFixedSize(true);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mListingsView.setLayoutManager(manager);

        SnapHelper helper = new LinearSnapHelper();
        helper.attachToRecyclerView(mListingsView);

        listingsAdapter = new ListingsAdapter(this, listings);
        mListingsView.setAdapter(listingsAdapter);

        mListingsView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    View centerView = helper.findSnapView(manager);
                    pos = manager.getPosition(centerView);
                    Listing currentListing = listings.get(pos);
                    mLocationText.setText(currentListing.getStreetAddress());
                }
            }
        });

//      TODO change to Query and check efficiency



        // Get Map View Bundle
        Bundle mapViewBundle = null;
        if(savedInstanceState != null) { mapViewBundle =
                savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY); }

        // Gets Map View from XML and creates it.
        mMapView = findViewById(R.id.search_map_view);
        Log.d(TAG, "mMapView.onCreate called");
        mMapView.onCreate(mapViewBundle);

        mMapView.getMapAsync(this);


//        Map<String, String> user = new HashMap<>();
//        user.put("profilePicture", "");
//        mFirestore.collection("users").document(mAuth.getUid()).set(user);
//        HashMap<String, Boolean> tags = new HashMap<>();
//        tags.put(Constants.Tags.DRIVEWAY, true);
//        tags.put(Constants.Tags.ALL_DAY_PARKING, true);
//        tags.put(Constants.Tags.SEDAN, true);
//        tags.put(Constants.Tags.MOTORCYCLE, true);
//        Listing l = new Listing("SF", "uid", 12.6, new Float(4.8),
//                21, "California", "67-89 Some Ave",
//                "Nice Parking Space at the heart of San Fransisco", tags);
//        listingsRef.add(l);
//        Intent searchActivityIntent = getIntent();
//        LatLng place = searchActivityIntent.getExtras().getParcelable("latLng");
//        Toast.makeText(SearchActivity.this, "Lat = " + place.latitude + " " +
//                "lng = " + place.longitude, Toast.LENGTH_LONG).show();

    }

    View.OnClickListener listener = (view) -> {
        switch (view.getId()){

            case R.id.search_listing_about_button:
                goToAboutActivity();
                break;

            case R.id.search_locate_fab:
                moveCamera(mPlace, DEFAULT_ZOOM, true);
                break;

            default:
                break;
        }
    };

    private void goToAboutActivity() {
        Intent aboutActivityIntent = new Intent(SearchActivity.this,
                AboutActivity.class);
        aboutActivityIntent.putExtra("pos", pos);
        aboutActivityIntent.putParcelableArrayListExtra("listings",
                (ArrayList<? extends Parcelable>) listings);
        startActivity(aboutActivityIntent);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search_activity_search:
                startNewSearch();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startNewSearch() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
            Toast.makeText(this, "Error: ", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i(TAG, "Place: " + place.getName());
                newSearch(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Toast.makeText(this,"Error", Toast.LENGTH_LONG).show();
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
                Toast.makeText(this, "Canclled", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void newSearch(Place place) {
        Intent newSearchIntent = getIntent();
        overridePendingTransition(0, 0);
        newSearchIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        newSearchIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        newSearchIntent.putExtra("latLng", place.getLatLng());
        finish();
        overridePendingTransition(0, 0);
        startActivity(newSearchIntent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable("latLng", mPlace);
        Bundle mapVieBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if(mapVieBundle != null){
            mapVieBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapVieBundle);
        }

        mMapView.onSaveInstanceState(mapVieBundle);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        styleMap();
        moveCamera(mPlace, DEFAULT_ZOOM, false);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
        Log.d(TAG, "ON START WAS CALLED");

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser == null){
            goToWelcomeActivity();
        }else {
            attachEventListener();
        }

    }

    private void attachEventListener() {

        if(listings.size() != 0) { listings.clear(); }
        listingsRef.addSnapshotListener(this, (queryDocumentSnapshots, e) -> {
                    if(e != null) {
                        Log.d("SearchActivity", "Error" + e.getMessage());
                    }

                    for(DocumentChange docChange : queryDocumentSnapshots.getDocumentChanges()){
                        switch (docChange.getType()){
                            case ADDED:
                                Listing newListing = docChange.getDocument().toObject(Listing.class);
                                newListing.setId(docChange.getDocument().getId());
//                                Log.d("SearchActivity", docChange.getDocument().getId());
                                if(listings.size() == 0) mLocationText.setText(newListing.getStreetAddress());
                                listings.add(newListing);
                                listingsMap.put(docChange.getDocument().getId(), newListing);
                                listingsAdapter.notifyDataSetChanged();
                                break;

                            case MODIFIED:
                                Listing oldListing = listingsMap.get(docChange.getDocument().getId());
                                Listing modifiedListing = docChange.getDocument().toObject(Listing.class);
                                oldListing.updateValues(modifiedListing);
                                listingsAdapter.notifyDataSetChanged();
                                break;

                            case REMOVED:
//                                TODO check efficiency
                                Listing removedListing = listingsMap.get(docChange.getDocument().getId());
                                listingsMap.remove(docChange.getDocument().getId());
                                listings.remove(removedListing);
                                listingsAdapter.notifyDataSetChanged();
                                break;
                        }

                    }

                });
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
        Log.d(TAG, "ON STOP WAS CALLED");
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
        Log.d(TAG, "ON PAUSE WAS CALLED");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        Log.d(TAG, "ON DESTROY WAS CALLED");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    private void goToWelcomeActivity(){
        Intent welcomeActivityIntent = new Intent(SearchActivity.this, WelcomeActivity.class);
        startActivity(welcomeActivityIntent);
        finish();
    }

    private void moveCamera(LatLng latLng, float zoom, Boolean animate){
        if(animate) mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        else mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    private void styleMap(){
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.map_style));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
    }





}
