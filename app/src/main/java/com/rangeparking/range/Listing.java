package com.rangeparking.range;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.GeoPoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Listing implements Parcelable{

    private String city;
    private String creator;
    private double price;
    private float rating;
    private int reviews;
    private String state;
    private String streetAddress;
    private String title;
    private String description;
    private Map<String, Boolean> tags;
    private GeoPoint location;
    private List<String> pictures;

    @Exclude
    private String id;

    public Listing() { };

    public Listing(String city, String creator, double price, float rating, int reviews,
                   String state, String streetAddress, String title, String description,
                   Map<String, Boolean> tags, GeoPoint location, List<String> pictures) {
        this.city = city;
        this.creator = creator;
        this.price = price;
        this.rating = rating;
        this.reviews = reviews;
        this.state = state;
        this.streetAddress = streetAddress;
        this.title = title;
        this.description = description;
        this.tags = tags;
        this.location = location;
        this.pictures = pictures;
    }

    protected Listing(Parcel in) {
        city = in.readString();
        creator = in.readString();
        price = in.readDouble();
        rating = in.readFloat();
        reviews = in.readInt();
        state = in.readString();
        streetAddress = in.readString();
        title = in.readString();
        description = in.readString();
        id = in.readString();
        tags = new HashMap<>();
        in.readMap(tags, Boolean.class.getClassLoader());
        Map<String, Double> location = new HashMap<>();
        in.readMap(location, Double.class.getClassLoader());
        this.location = new GeoPoint(location.get("lat"), location.get("lng"));
        pictures = new ArrayList<>();
        in.readStringList(pictures);
    }

    @Exclude
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(city);
        dest.writeString(creator);
        dest.writeDouble(price);
        dest.writeFloat(rating);
        dest.writeInt(reviews);
        dest.writeString(state);
        dest.writeString(streetAddress);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(id);
        dest.writeMap(tags);
        Map<String, Double> geo = new HashMap<>();
        geo.put("lat", location.getLatitude());
        geo.put("lng", location.getLongitude());
        dest.writeMap(geo);
        dest.writeStringList(pictures);
    }

    @Exclude
    @Override
    public int describeContents() { return 0; }

    @Exclude
    public static final Creator<Listing> CREATOR = new Creator<Listing>() {
        @Override
        public Listing createFromParcel(Parcel in) {
            return new Listing(in);
        }

        @Override
        public Listing[] newArray(int size) {
            return new Listing[size];
        }
    };

    public String getCity() { return city; }

    public void setCity(String city) { this.city = city; }

    public String getCreator() { return creator; }

    public void setCreator(String creator) { this.creator = creator; }

    public double getPrice() { return price; }

    public void setPrice(double price) { this.price = price; }

    public float getRating() { return rating; }

    public void setRating(float rating) { this.rating = rating; }

    public int getReviews() { return reviews; }

    public void setReviews(int reviews) { this.reviews = reviews; }

    public String getState() { return state; }

    public void setState(String state) { this.state = state; }

    public String getStreetAddress() { return streetAddress; }

    public void setStreetAddress(String streetAddress) { this.streetAddress = streetAddress; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public Map<String, Boolean> getTags() { return tags; }

    public void setTags(Map<String, Boolean> tags) { this.tags = tags; }

    public GeoPoint getLocation() { return location; }

    public void setLocation(GeoPoint location) { this.location = location; }

    public List<String> getPictures() { return pictures; }

    public void setPictures(List<String> pictures) { this.pictures = pictures; }

    public static Creator<Listing> getCREATOR() {
        return CREATOR;
    }

    @Exclude
    public void setId(String id) { this.id = id; }

    @Exclude
    public String getId() { return id; }

//    @Exclude
//    public Map<String, Object> toMap() {
//        Map<String, Object> result = toFirebase();
//        result.put("id", id);
//
//        return result;
//    }
//
//    @Exclude
//    public Map<String, Object> toFirebase() {
//        HashMap<String, Object> result = new HashMap<>();
//        result.put("city", city);
//        result.put("creator", creator);
//        result.put("title", title);
//        result.put("price", price);
//        result.put("rating", rating);
//        result.put("reviews", reviews);
//        result.put("state", state);
//        result.put("streetAddress", streetAddress);
//        result.put("tags", tags);
//
//        return result;
//    }

//    @Exclude
//    public void fromMap(Map<String, Object> fromMap){
//        city = (String) fromMap.get("city");
//        creator = (String) fromMap.get("creator");
//        title = (String) fromMap.get("title");
//        price = (Double) fromMap.get("price");
//        rating = (Float) fromMap.get("rating");
//        reviews = (Integer) fromMap.get("reviews");
//        state = (String) fromMap.get("state");
//        streetAddress = (String) fromMap.get("streetAddress");
//        tags = (Map<String, Boolean>) fromMap.get("tags");
//        id = (String) fromMap.get("id");
//    }


    /**
     * TODO check efficiency | is it needed to compare to previous data
     * Updates the current listing with new data
     * @param updatedListing listing object with updated data
     */
    @Exclude
    public void updateValues(Listing updatedListing){
        city = updatedListing.city;
        creator = updatedListing.creator;
        price = updatedListing.price;
        rating = updatedListing.rating;
        reviews = updatedListing.reviews;
        state = updatedListing.state;
        streetAddress = updatedListing.streetAddress;
        title = updatedListing.title;
        description = updatedListing.description;
        tags = updatedListing.tags;
        location = updatedListing.location;
    }

}
