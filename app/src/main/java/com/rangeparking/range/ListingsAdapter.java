package com.rangeparking.range;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class ListingsAdapter extends RecyclerView.Adapter<ListingsAdapter.ListingHolder> {

//    List of listings
    private List<Listing> listings;
    private Context context;
    private FirebaseFirestore mFirestore = FirebaseFirestore.getInstance();
    private CollectionReference usersRef = mFirestore.collection("users");

//    public constructor for adapter
    public ListingsAdapter(Context context, List<Listing> listings) {
        this.listings = listings;
        this.context = context;
    }

    /*
    takes ViewGroup which is the recylerview, takes the context (searchActivity) and obtains
    layoutInflater and inflates layout needs parent to know where to place view and false returns
    the single listing view instead of the entire recylerview, the parent specifies the layout params.
    the view is passed to a view holder and is returned. This method is called every time a row
    is generated
    */
    @NonNull
    @Override
    public ListingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listing_layout, parent, false);

        return new ListingHolder(view);
    }

    /*
    Returns the the view holder that holds the current item in list and binds data
    */
    @Override
    public void onBindViewHolder(@NonNull ListingHolder holder, int position) {
//        holder.progressBar.setVisibility(View.VISIBLE);
        Listing model = listings.get(position);
        holder.price.setText("$" +model.getPrice() + "/hr");
        holder.reviews.setText("(" + model.getReviews() + ")");
        holder.listingTitle.setText(model.getTitle());
        holder.ratingBar.setRating(model.getRating());
        if(model.getCreator().length() > 4) loadImages(holder, model);
    }

    private void loadImages(ListingHolder holder, Listing model){

        Glide.with(context)
            .load(model.getPictures().get(0))
            .apply(new RequestOptions()
            .centerCrop())
            .into(holder.listingImage);

        usersRef.document(model.getCreator()).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    String profilePictureUrl = (String) document.get("profilePictureUrl");
                    Glide
                        .with(context)
                        .load(profilePictureUrl)
                        .apply(new RequestOptions()
                        .centerCrop())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                        Target<Drawable> target,
                                                        boolean isFirstResource) {
                                holder.progressBar.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model,
                                                           Target<Drawable> target,
                                                           DataSource dataSource,
                                                           boolean isFirstResource) {
                                holder.progressBar.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.listingProfilePicture);
//                    Log.d("SomeThing", "DocumentSnapshot data: " + document.getData());
                } else {
//                    Log.d(TAG, "No such document");
                }
            } else {
//                Log.d(TAG, "get failed with ", task.getException());
            }
        });
    }

    @Override
    public int getItemCount() {
        return listings.size();
    }

    public class ListingHolder extends RecyclerView.ViewHolder {

        TextView price;
        TextView listingTitle;
        TextView reviews;
        ImageView listingImage;
        ImageView listingProfilePicture;
        RatingBar ratingBar;
        ProgressBar progressBar;

//        Creates the views by finding resource in xml
        public ListingHolder(View itemView) {
            super(itemView);

            price = itemView.findViewById(R.id.listing_price);
            listingTitle = itemView.findViewById(R.id.listing_title);
            reviews = itemView.findViewById(R.id.listing_reviews);
            listingImage = itemView.findViewById(R.id.listing_image);
            listingProfilePicture = itemView.findViewById(R.id.listing_profile_picture);
            ratingBar = itemView.findViewById(R.id.listing_rating_bar);
            progressBar = itemView.findViewById(R.id.listing_progressBar);
        }

    }
}
