package com.rangeparking.range;

class User {

    private String uid;
    private String firstName;
    private String lastName;
    private String profilePictureUrl;

    public User() {
//    required empty constructor
    }

    public User(String uid, String firstName, String lastName, String profilePictureUrl) {
        this.uid = uid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.profilePictureUrl = profilePictureUrl;
    }

    public String getUid() { return uid; }

    public void setUid(String uid) { this.uid = uid; }

    public String getFirstName() { return firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getProfilePictureUrl() { return profilePictureUrl; }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }
}
