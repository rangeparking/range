package com.rangeparking.range;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WelcomeActivity extends AppCompatActivity {

//    Constants

//    Widgets
//      TextViews
    private TextView mSignInText,
        mSuggestionText,
        mSkipText,
        mContinueWithSocialText;
//      EditTexts
    private EditText mFirstNameEditText,
            mLastNameEditText,
            mEmailEditText,
            mPasswordEditText;
//      ImageViews
    private ImageView mFirstNameCheck,
            mLastNameCheck,
            mEmailCheck,
            mPasswordCheck,
            mSkipNavArrow;
//      ScrollViews
    private ScrollView mScrollView;
//      Buttons
    private Button mSignUpButton, mLoginButton;

//    Firebase
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

//    Vars
    private boolean mValidEmail = false;
    private boolean mValidPassword = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        mSignInText = findViewById(R.id.signInText);
        mFirstNameEditText = findViewById(R.id.firstNameFieldWelcome);
        mLastNameEditText = findViewById(R.id.lastNameFieldWelcome);
        mEmailEditText = findViewById(R.id.emailFieldWelcome);
        mPasswordEditText = findViewById(R.id.passwordFieldWelcome);
        mFirstNameCheck = findViewById(R.id.firstNameCheck);
        mLastNameCheck = findViewById(R.id.lastNameCheck);
        mEmailCheck = findViewById(R.id.emailCheck);
        mPasswordCheck = findViewById(R.id.passwordCheck);
        mSignUpButton = findViewById(R.id.signUpButtonWelcome);
        mLoginButton = findViewById(R.id.logInButtonWelcome);
        mSuggestionText = findViewById(R.id.passwordSuggestionText);
        mScrollView = findViewById(R.id.scrollViewWelcome);
        mSkipText = findViewById(R.id.skip);
        mSkipNavArrow = findViewById(R.id.skipNavigationArrow);
        mContinueWithSocialText = findViewById(R.id.continueWithFacebookorGoogleText);



        SpannableString signInSpannableString = new SpannableString("Sign Up \nfor Range");
        ForegroundColorSpan fcsPurple = new ForegroundColorSpan(ContextCompat
                .getColor(this, R.color.colorRangeTitle));
        signInSpannableString.setSpan(fcsPurple, 9, signInSpannableString.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        mSignInText.setText(signInSpannableString);

        mFirstNameEditText.addTextChangedListener(new GenericTextWatcher(mFirstNameEditText));
        mFirstNameEditText.setOnFocusChangeListener(focusChangeListener);
        mLastNameEditText.addTextChangedListener(new GenericTextWatcher(mLastNameEditText));
        mLastNameEditText.setOnFocusChangeListener(focusChangeListener);
        mEmailEditText.addTextChangedListener(new GenericTextWatcher(mEmailEditText));
        mEmailEditText.setOnFocusChangeListener(focusChangeListener);
//        mEmailEditText.setOnClickListener(clickListener);
        mPasswordEditText.addTextChangedListener(new GenericTextWatcher(mPasswordEditText));
        mPasswordEditText.setOnFocusChangeListener(focusChangeListener);
//        mPasswordEditText.setOnClickListener(clickListener);
        mSignUpButton.setOnClickListener(clickListener);
        mLoginButton.setOnClickListener(clickListener);
        mSkipText.setOnClickListener(clickListener);
        mSkipNavArrow.setOnClickListener(clickListener);
        mContinueWithSocialText.setOnClickListener(clickListener);

        mEmailEditText.getCompoundDrawables();
    }

    private View.OnClickListener clickListener = (view) -> {
        switch (view.getId()){
            case R.id.signUpButtonWelcome:
                if(!mValidEmail) Toast.makeText(this, "Invalid Email",
                        Toast.LENGTH_SHORT).show();
                break;

            case R.id.logInButtonWelcome:
                goToEmailOrPhoneLoginActivity();
                break;

            case R.id.skip:
                signInAnonmously();
                break;

            case R.id.skipNavigationArrow:
                signInAnonmously();
                break;

            case R.id.continueWithFacebookorGoogleText:
                goToSignInWithSocialActivity();
                break;

//            case R.id.emailFieldWelcome:
//                mScrollView.scrollTo(0, mScrollView.getBottom());
//                break;
//
//            case R.id.passwordFieldWelcome:
//                mScrollView.scrollTo(0, mScrollView.getBottom());
//                break;
        }
    };

    private void goToSignInWithSocialActivity() {
        Intent signInWithSocialActivitylIntent = new Intent(WelcomeActivity.this,
                SignInWithSocialActivity.class);
        startActivity(signInWithSocialActivitylIntent);
    }

    private void signInAnonmously() {
//        TODO anonymous sign in
        goToMainActivity();
    }

    private void goToEmailOrPhoneLoginActivity() {
        Intent emailOrPhoneLoginIntent = new Intent(WelcomeActivity.this,
                EmailOrPhoneLoginActivity.class);
        startActivity(emailOrPhoneLoginIntent);
    }


    private View.OnFocusChangeListener focusChangeListener = (view, hasFocus) -> {

        switch (view.getId()){
            case R.id.passwordFieldWelcome:
                if(hasFocus) {
                    mScrollView.scrollTo(0, mScrollView.getBottom());
                    mSuggestionText.setText(getString(R.string.PasswordSuggestion));
                    mSuggestionText.setVisibility(View.VISIBLE);
                }
                else mSuggestionText.setVisibility(View.INVISIBLE);
                break;
            case R.id.emailFieldWelcome:
                if(hasFocus && !mValidEmail){
                    mScrollView.scrollTo(0, mScrollView.getBottom());
                    mSuggestionText.setText("Please Enter a valid email");
                    mSuggestionText.setVisibility(View.VISIBLE);
                }else mSuggestionText.setVisibility(View.INVISIBLE);
                break;
        }

    };

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null) goToMainActivity();
    }

    private void goToMainActivity() {
        Intent mainActivityIntent = new Intent(WelcomeActivity.this, MainActivity.class);
        startActivity(mainActivityIntent);
        finish();
    }



    private class GenericTextWatcher implements TextWatcher {

        private final Pattern VALID_EMAIL_ADDRESS_REGEX =
                Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
                        Pattern.CASE_INSENSITIVE);

        private EditText editText;

        GenericTextWatcher(EditText editText){
            this.editText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            switch (editText.getId()){

                case R.id.firstNameFieldWelcome: checkStringNotEmpty(s, mFirstNameCheck);
                    break;
                case R.id.lastNameFieldWelcome: checkStringNotEmpty(s, mLastNameCheck);
                    break;
                case R.id.emailFieldWelcome: validateEmail(s.toString());
                    break;
                case R.id.passwordFieldWelcome: validatePassword(s.toString());
                    break;
                default:
                    break;
            }

        }

        private void validateEmail(String s) {
            if(s.equals("")) {
                mValidEmail = false;
                mEmailCheck.setVisibility(View.INVISIBLE);
            }
            else if(validEmail(s)) {
                mValidEmail = true;
                mEmailCheck.setImageResource(R.drawable.ic_round_done_24px);
                mEmailCheck.setVisibility(View.VISIBLE);
            }
            else {
                mValidEmail = false;
                mEmailCheck.setImageResource(R.drawable.ic_round_close_24px);
                mEmailCheck.setVisibility(View.VISIBLE);
            }
        }

        private boolean validEmail(String emailStr) {
            Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
            return matcher.find();
        }

        private void checkStringNotEmpty(Editable s, ImageView check){
            if(!s.toString().equals("")) check.setVisibility(View.VISIBLE);
            else check.setVisibility(View.INVISIBLE);
        }

        private void validatePassword(String s){
            if(s.equals("")) {
                mValidPassword = false;
                mPasswordCheck.setVisibility(View.INVISIBLE);
            }
            else if(s.length() >= 8) {
                mValidPassword = true;
                mPasswordCheck.setImageResource(R.drawable.ic_round_done_24px);
                mPasswordCheck.setVisibility(View.VISIBLE);
            }
            else {
                mValidPassword = false;
                mPasswordCheck.setImageResource(R.drawable.ic_round_close_24px);
                mPasswordCheck.setVisibility(View.VISIBLE);
            }
        }
    }
}
